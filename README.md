# obtpa-gui

[![Publish Npm Package And Deploy Github Pages](https://github.com/obtp/obtpa-gui/actions/workflows/publish-npm-package-and-deploy-gh-pages.yml/badge.svg)](https://github.com/obtp/obtpa-gui/actions/workflows/publish-npm-package-and-deploy-gh-pages.yml)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgithub.com%2Fobtp%2Fobtpa-gui.svg?type=shield)](https://app.fossa.com/projects/git%2Bgithub.com%2Fobtp%2Fobtpa-gui?ref=badge_shield)
[![Gitter](https://badges.gitter.im/obtp/community.svg)](https://gitter.im/obtp/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
[![ko-fi](https://img.shields.io/badge/donate-sponsors-ea4aaa.svg?logo=ko-fi)](https://ko-fi.com/X8X66DATO)

**Buildtopia GUI is a set of React components that comprise the interface for creating and running Buildtopia projects**

## Try Buildtopia Online

Buildtopia Online: [https://obtp.github.io/obtpa-gui/develop/](https://obtp.github.io/obtpa-gui/develop/)

![screenshot1](./docs/screenshot1.png)
![screenshot2](./docs/screenshot2.png)

## Getting Start

Visit the wiki: [https://wiki.obtpa.cc](https://wiki.obtpa.cc)

## Join chat

- Gitter: [https://gitter.im/obtp/community](https://gitter.im/obtp/community?utm_source=share-link&utm_medium=link&utm_campaign=share-link)

- QQ 群 (for chinese): 933484739

## Donate

Buy me a cup of coffee.

- Ko-fi (PayPal):

    [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/X8X66DATO)

- 支付宝:

    ![alipayQRCode](./docs/alipayQRCode.png)

## Bug Report

You can submit the bug log in issues of this project.


## License
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgithub.com%2Fobtp%2Fobtpa-gui.svg?type=large)](https://app.fossa.com/projects/git%2Bgithub.com%2Fobtp%2Fobtpa-gui?ref=badge_large)
