import 'obtpa-blocks/arduino_compressed';
import 'obtpa-blocks/python_compressed';

const getLanguageFromDeviceType = deviceType => {
    if (deviceType === 'arduino') {
        return 'cpp';
    } else if (deviceType === 'microbit' || deviceType === 'mpython') {
        return 'python';
    }
    return 'null';
};

const getGeneratorNameFromDeviceType = deviceType => {
    if (deviceType === 'arduino') {
        return 'Arduino';
    } else if (deviceType === 'microbit' || deviceType === 'mpython') {
        return 'Python';
    }
    return 'null';
};

export {
    getLanguageFromDeviceType,
    getGeneratorNameFromDeviceType
};
